# Changelog

All notable changes to this repository will be documented in this file.

## 2024-11-05

### Changed

- Renamed project `grapeyard-labs/books/the-lazy-game-master` to `grapeyard-labs/books/the-lazy-game-master-compendium`.

## 2024-06-01

### Added

- Added icon for project `grapeyard-labs/books/the-texbrewery` (including a framed version of the icon).

### Changed

- Revised the icon for project `grapeyard-labs/books/the-lazy-game-master` to represent a sleeping die wearing a wizard's hat.

## 2023-11-24

### Added

- Added icon for subgroup `grapeyard-labs/books`.
- Added icon for project `grapeyard-labs/books/the-lazy-game-master` (including a framed version of the icon).
- Added unframed version of the icon for project `grapeyard-labs/libs/grapeyard-icons`.

### Changed

- Downsized the grapes in the icon for subgroup `grapeyard-labs/apps`.
- Revised the icon for subgroup `grapeyard-labs/libs` to represent a laptop used for code development.
- Adjusted the style of the icon for project `grapeyard-labs/libs/grapeyard-icons` to match the style of the icon for project `grapeyard-labs/libs/nuke-launchpad`.
- Inverted the color of the rocket in the unframed version of the icon for project `grapeyard-labs/libs/nuke-launchpad`.

## 2023-07-05

### Added

- Added icon for group `grapeyard-labs` (including a framed version of the icon).
- Added icon for subgroup `grapeyard-labs/apps`.
- Added icon for subgroup `grapeyard-labs/libs`.
- Added icon for project `grapeyard-labs/libs/grapeyard-icons`.
- Added icon for project `grapeyard-labs/libs/nuke-launchpad` (including a framed version of the icon).
